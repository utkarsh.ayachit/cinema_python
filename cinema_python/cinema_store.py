"""
    Module defining classes and methods for managing cinema data storage.
"""

import sys
import json
import os.path
import re
import itertools
import weakref
import PIL.Image
import numpy as np

class Document(object):
    """
    This refers to a document in the cinema data storage. A document is
    uniquely identified by a 'descriptor'. A descriptor is a dictionary
    with key-value pairs, where key is a parameter name and value is the
    value for that particular parameter.
    TODO:
    A document can have arbitrary data (as 'data') and meta-data (as
    'attributes') associated with it. At the moment we are assuming
    stored images and are ignoring the attributes.
    """
    def __init__(self, descriptor, data=None):
        self.__descriptor = descriptor
        self.__data = data
        self.__attributes = None

    @property
    def descriptor(self):
        """A document descriptor is a unique
        identifier for the document. It is a dict with key value pairs. The
        descriptor cannot be changed once the document has been instantiated."""
        return self.__descriptor

    @property
    def data(self):
        """Data associated with the document."""
        return self.__data

    @data.setter
    def data(self, val):
        self.__data = val

    @property
    def attributes(self):
        """Attributes are arbitrary meta-data associated with the document.
        If no attributes are present, it is set to None. When present,
        attributes are a dict with arbitrary meta-data relevant to the application.
        """
        return self.__attributes

    @attributes.setter
    def attributes(self, attrs):
        self.__attributes = attrs

class Store(object):
    """
    API for cinema stores. A store is a collection of Documents,
    with API to add, find, and access them. This class is an abstract class
    defining the API and storage independent logic. Storage specific
    subclasses handle the 'database' access.

    The design of cinema store is based on the following principles:

    The store comprises of documents (Document instances). Each document has a
    unique set of parameters, aka a "descriptor" associated with it. This
    can be thought of as the 'unique key' in database terminology.

    One defines the parameters (contents of the descriptor) for documents
    on the store itself. The set of them is is referred to as 'parameter_list'.
    One uses 'add_parameter()' calls to add new parameter definitions for a new
    store instance.

    Users insert documents in the store using 'insert'. One can find
    document(s) using 'find' which returns a generator (or cursor) allow users
    to iterate over all match documents.
    """

    def __init__(self):
        self.__metadata = None
        self.__parameter_list = {}
        self.__loaded = False
        self.__parameter_associations = {}
        self.__type_specs = {}

    @property
    def parameter_list(self):
        """
        The parameter list is the set of variables and their values that the
        documents in the store vary over. """
        return self.__parameter_list

    def _parse_parameter_type(self, name, properties):
        #look for hints about document type relations
        if 'Z' in self.__type_specs:
            Zs = self.__type_specs['Z']
        else:
            Zs = []
        if 'types' in properties:
            for x in range(0, len(properties['types'])):
                if properties['types'][x] == 'depth':
                    value = properties['values'][x]
                    newentry = [name, value]
                    Zs.append(newentry)
        if len(Zs) > 0:
            self.__type_specs['Z'] = Zs

    def _set_parameter_list(self, val):
        """For use by subclasses alone"""
        self.__parameter_list = val
        for name in self.__parameter_list:
            self._parse_parameter_type(name, self.__parameter_list[name])

    def add_parameter(self, name, properties):
        """Add a parameter.
        :param name: Name for the parameter.
        :param properties: Keyword arguments can be used to associate miscellaneous
        meta-data with this parameter.
        """
        #if self.__loaded:
        #    raise RuntimeError("Updating parameters after loading/creating a store is not supported.")
        # TODO: Err, except when it is, in the important case of adding new time steps to a collection.
        # I postulate it is always OK to add safely to outermost parameter (loop).
        self.__parameter_list[name] = properties
        self._parse_parameter_type(name, properties)

    def get_parameter(self, name):
        return self.__parameter_list[name]

    def get_complete_descriptor(self, partial_desc):
        """
        Convenience method that expands an incomplete list of parameters into
        the full set using default values for the missing variables.
        TODO: doesn't make sense with bifurcation (dependencies), when SFS supports them remove
        """
        full_desc = dict()
        for name, properties in self.parameter_list.items():
            if properties.has_key("default"):
                full_desc[name] = properties["default"]
        full_desc.update(partial_desc)
        return full_desc

    def get_default_type(self):
        """ subclasses override this if they know more """
        return "RGB"

    def determine_type(self, desc):
        #try any assigned mappings (for example color='depth' then 'Z')
        for typename, checks in self.__type_specs.items():
            for check in checks:
                name = check[0]
                conditions = check[1]
                if name in desc and desc[name] in conditions:
                    return typename
        #no takers, use the default for this store
        typename = self.get_default_type()
        return typename


    @property
    def parameter_associations(self):
        return self.__parameter_associations

    def _set_parameter_associations(self, val):
        """For use by subclasses alone"""
        self.__parameter_associations = val

    @property
    def metadata(self):
        """
        Auxiliary data about the store itself. An example is hints that help the
        viewer app know how to interpret this particular store.
        """
        return self.__metadata

    @metadata.setter
    def metadata(self, val):
        self.__metadata = val

    def add_metadata(self, keyval):
        if not self.__metadata:
            self.__metadata = {}
        self.__metadata.update(keyval)

    def create(self):
        """
        Creates an empty store.
        Subclasses must extend this.
        """
        assert not self.__loaded
        self.__loaded = True

    def load(self):
        """
        Loads contents on the store (but not the documents).
        Subclasses must extend this.
        """
        assert not self.__loaded
        self.__loaded = True

    def find(self, q=None):
        """
        Return iterator to all documents that match query q.
        Should support empty query or direct values queries e.g.

        for doc in store.find({'phi': 0}):
            print doc.data
        for doc in store.find({'phi': 0, 'theta': 100}):
            print doc.data

        """
        raise RuntimeError("Subclasses must define this method")

    def insert(self, document):
        """
        Inserts a new document.
        Subclasses must extend this.
        """
        if not self.__loaded:
            self.create()

    def assign_parameter_dependence(self, dep_param, param, on_values):
        """
        mark a particular parameter as being explorable only for a subset
        of the possible values of another.

        For example given parameter 'appendage type' which might have
        value 'foot' or 'flipper', a dependent parameter might be 'shoe type'
        which only makes sense for 'feet'. More to the point we use this
        for 'layers' and 'fields' in composite rendering of objects in a scene
        and the color settings that each object is allowed to take.
        """
        self.__parameter_associations.setdefault(dep_param, {}).update(
        {param: on_values})

    def dependencies_satisfied(self, dep_param, descriptor):
        """
        Check if that values in decriptor satisfy all of the dependencies
        of dep_param.
        Return true if no dependencies to satisfy.
        Return false if dependency of dependency fails.
        """
        if not dep_param in self.__parameter_associations:
            return True
        for dep in self.__parameter_associations[dep_param]:
            if not dep in descriptor:
                return False
            if not descriptor[dep] in self.__parameter_associations[dep_param][dep]:
                return False
            if not self.dependencies_satisfied(dep, descriptor):
                return False
        return True

    def add_layer(self, name, properties):
        """
        A Layer boils down to an image of something in the scene, and only
        that thing, along with the depth at each pixel. Layers (note the
        plural) can be composited back together by a viewer.
        """
        properties['type'] = 'option'
        properties['islayer'] = 'yes'
        self.add_parameter(name, properties)

    def add_sublayer(self, name, properties, parent_layer, parents_value):
        """
        An example of a layer is an isocontour display. An examples of sublayer
        is the particular isovalues.
        """
        self.add_layer(name, properties)
        self.assign_parameter_dependence(name, parent_layer, parents_value)

    def add_field(self, name, properties, parent_layer, parents_values):
        """
        A field is a component of the final color for a layer. Examples include:
        depth, normal, color, scalar values.
        """
        properties['type'] = 'hidden'
        properties['isfield'] = 'yes' #better yet, add a flag elsewhere in json
        self.add_parameter(name, properties)
        self.assign_parameter_dependence(name, parent_layer, parents_values)

    def iterate(self, parameters=None, fixedargs=None):
        """
        Run through all combinarions of parameter/value pairs without visiting
        any combinations that do not satisfy dependencies among them.
        Parameters, if supplied, is a list of parameter names to enforce an ordering.
        Fixed arguments, if supplied are are parameter/value pairs that we want
        to hold constant in the exploration.
        """
        dependencies = self.parameter_associations
        #print "DEPS", dependencies

        #prepare to iterate through all the possibilities, in order if one is given
        param_names = parameters if parameters else self.parameter_list.keys()
        params = []
        values = []
        dep_params = []
        for name in param_names:
            vals = self.get_parameter(name)['values']
            if fixedargs and name in fixedargs:
                continue
            params.append(name)
            values.append(vals)

        #the algorithm is to iterate through all combinations, and remove
        #the impossible ones. I use a set to avoid redundant combinations.
        #In order to use the set I serialize to make something hashable.
        #Then I insert into a list to preserve the (hopefully optimized) order.
        ok_descs = set()
        ordered_descs = []
        for element in itertools.product(*values):
            descriptor = dict(itertools.izip(params, element))

            if fixedargs != None:
                descriptor.update(fixedargs)

            ok_params = []
            ok_vals = []

            ok_desc = {}
            for param, value in descriptor.iteritems():
                if self.dependencies_satisfied(param, descriptor):
                    ok_desc.update({param:value})

            strval = json.dumps(ok_desc, sort_keys=True)
            if not strval in ok_descs:
                ok_descs.add(strval)
                ordered_descs.append(ok_desc)

        for descriptor in ordered_descs:
            yield descriptor

class FileStore(Store):
    """Implementation of a store based on named files and directories."""

    def __init__(self, dbfilename=None):
        super(FileStore, self).__init__()
        self.__filename_pattern = None
        self.__dbfilename = dbfilename if dbfilename \
                else os.path.join(os.getcwd(), "info.json")

    def create(self):
        """creates a new file store"""
        super(FileStore, self).create()
        self.save()

    def load(self):
        """loads an existing filestore"""
        super(FileStore, self).load()
        with open(self.__dbfilename, mode="rb") as file:
            info_json = json.load(file)
            #for legacy reasons, the parameters are called
            #arguments" in the files
            self._set_parameter_list(info_json['arguments'])
            self.metadata = info_json['metadata']
            self.filename_pattern = info_json['name_pattern']
            self._set_parameter_associations(info_json['associations'])

    def save(self):
        """ writes out a modified file store """
        info_json = dict(
                arguments = self.parameter_list,
                name_pattern = self.filename_pattern,
                metadata = self.metadata,
                associations = self.parameter_associations
                )
        dirname = os.path.dirname(self.__dbfilename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        with open(self.__dbfilename, mode="wb") as file:
            json.dump(info_json, file)

    @property
    def filename_pattern(self):
        """
        Files corresponding to Documents are arranged on disk
        according the the directory and filename structure described
        by the filename_pattern. The format is a regular expression
        consisting of parameter names enclosed in '{' and '}' and
        separated by spacers. "/" spacer characters produce sub
        directories.
        """
        return self.__filename_pattern

    @filename_pattern.setter
    def filename_pattern(self, val):
        self.__filename_pattern = val

        #choose default data type in the store based on file extension
        self._default_type = 'RGB'
        if val[val.rfind("."):] == '.txt':
            self._default_type = 'TXT'

    def get_default_type(self):
        """ overridden to use the filename pattern to determine default type """
        return self._default_type

    def _get_filename(self, desc):
        dirname = os.path.dirname(self.__dbfilename)

        #find filename modulo any dependent parameters
        fixed = self.filename_pattern.format(**desc)
        base, ext = os.path.splitext(fixed)

        #add any dependent parameters
        for dep in self.parameter_associations.keys():
            if dep in desc:
                base = base + "/" + dep + "=" + str(desc[dep])

        #determine file type for this document
        doctype = self.determine_type(desc)
        if doctype == "Z":
            ext = ".im"

        fullpath = os.path.join(dirname, base+ext)
        return fullpath

    def insert(self, document):
        super(FileStore, self).insert(document)

        fname = self._get_filename(document.descriptor)

        dirname = os.path.dirname(fname)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        if not document.data == None:
            doctype = self.determine_type(document.descriptor)
            if doctype == 'RGB':
                imageslice = document.data
                pimg = PIL.Image.fromarray(imageslice)
                pimg.save(fname)
            elif doctype == 'Z':
                imageslice = document.data
                pimg = PIL.Image.fromarray(imageslice)
                pimg.save(fname) #beside PIL.im, is there a standard for depth images?
            else:
                with open(fname, mode='w') as file:
                    file.write(document.data)

    def _load_data(self, doc_file, descriptor):
        doctype = self.determine_type(descriptor)
        try:
            if doctype == 'RGB':
                im = PIL.Image.open(doc_file)
                data = np.array(im, np.uint8).reshape(im.size[1], im.size[0], 3)
            elif doctype == 'Z':
                im = PIL.Image.open(doc_file)
                data = np.array(im, np.float32).reshape(im.size[1], im.size[0])
            else:
                with open(doc_file, "r") as file:
                    data = file.read()
        except IOError:
            data = None
        doc = Document(descriptor, data)
        doc.attributes = None
        return doc

    def find(self, q=None):
        #def matches(complete_desc, target_desc):
        #    for param_name in target_desc.keys():
        #        if not param_name in complete_desc:
        #            return False
        #        if complete_desc[param_name] != target_desc[param_name]:
        #            return False
        #    return True
        q = q if q else dict()
        target_desc = q

        for possible_desc in self.iterate(fixedargs=target_desc):
            #if matches(possible_desc, target_desc): (fixedargs makes unnecessary)
            filename = self._get_filename(possible_desc)
            yield self._load_data(filename, possible_desc)


class SingleFileStore(Store):
    """Implementation of a store based on a single volume file (image stack)."""

    def __init__(self, dbfilename=None):
        super(SingleFileStore, self).__init__()
        self.__dbfilename = dbfilename if dbfilename \
                else os.path.join(os.getcwd(), "info.json")
        self._volume = None
        self._needWrite = False
        self.add_metadata({"store_type" : "SFS"})

    def __del__(self):
        if self._needWrite:
            import vtk
            vw = vtk.vtkXMLImageDataWriter()
            vw.SetFileName(self._vol_file)
            vw.SetInputData(self._volume)
            vw.Write()

    def create(self):
        """creates a new file store"""
        super(SingleFileStore, self).create()
        self.save()

    def load(self):
        """loads an existing filestore"""
        super(SingleFileStore, self).load()
        with open(self.__dbfilename, mode="rb") as file:
            info_json = json.load(file)
            self._set_parameter_list(info_json['arguments'])
            self.metadata = info_json['metadata']

    def save(self):
        """ writes out a modified file store """
        info_json = dict(
                arguments = self.parameter_list,
                metadata = self.metadata
                )
        dirname = os.path.dirname(self.__dbfilename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        with open(self.__dbfilename, mode="wb") as file:
            json.dump(info_json, file)

    def _get_numslices(self):
        slices = 0
        for name in sorted(self.parameter_list.keys()):
            numvals = len(self.get_parameter(name)['values'])
            if slices == 0:
                slices = numvals
            else:
                slices = slices * numvals
        return slices

    def compute_sliceindex(self, descriptor):
        #find position of descriptor within the set of slices
        #TODO: algorithm is dumb, but consisent with find (which is also dumb)
        args = []
        values = []
        ordered = sorted(self.parameter_list.keys())
        for name in ordered:
            vals = self.get_parameter(name)['values']
            args.append(name)
            values.append(vals)
        index = 0
        for element in itertools.product(*values):
            desc = dict(itertools.izip(args, element))
            fail = False
            for k,v in descriptor.items():
                if desc[k] != v:
                    fail = True
            if not fail:
                return index
            index = index + 1

    def get_sliceindex(self, document):
        desc = self.get_complete_descriptor(document.descriptor)
        index = self.compute_sliceindex(desc)
        return index

    def _insertslice(self, vol_file, index, document):
        volume = self._volume
        width = document.data.shape[0]
        height = document.data.shape[1]
        if not volume:
            import vtk
            slices = self._get_numslices()
            volume = vtk.vtkImageData()
            volume.SetExtent(0, width-1, 0, height-1, 0, slices-1)
            volume.AllocateScalars(vtk.VTK_UNSIGNED_CHAR, 3)
            self._volume = volume
            self._vol_file = vol_file

        imageslice = document.data
        imageslice = imageslice.reshape(width*height, 3)

        from vtk.numpy_interface import dataset_adapter as dsa
        image = dsa.WrapDataObject(volume)
        oid = volume.ComputePointId([0,0,index])
        nparray = image.PointData[0]
        nparray[oid:oid+(width*height)] = imageslice

        self._needWrite = True

    def insert(self, document):
        super(SingleFileStore, self).insert(document)

        index = self.get_sliceindex(document)

        if not document.data == None:
            dirname = os.path.dirname(self.__dbfilename)
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            vol_file = os.path.join(dirname, "cinema.vti")
            self._insertslice(vol_file, index, document)

    def _load_slice(self, q, index, desc):

        if not self._volume:
            import vtk
            dirname = os.path.dirname(self.__dbfilename)
            vol_file = os.path.join(dirname, "cinema.vti")
            vr = vtk.vtkXMLImageDataReader()
            vr.SetFileName(vol_file)
            vr.Update()
            volume = vr.GetOutput()
            self._volume = volume
            self._vol_file = vol_file
        else:
            volume = self._volume

        ext = volume.GetExtent()
        width = ext[1]-ext[0]
        height = ext[3]-ext[2]

        from vtk.numpy_interface import dataset_adapter as dsa
        image = dsa.WrapDataObject(volume)

        oid = volume.ComputePointId([0, 0, index])
        nparray = image.PointData[0]
        imageslice = np.reshape(nparray[oid:oid+width*height], (width,height,3))

        doc = Document(desc, imageslice)
        doc.attributes = None
        return doc

    def find(self, q=None):
        #TODO: algorithm is dumb, but consisent with compute_sliceindex (which is also dumb)
        q = q if q else dict()
        args = []
        values = []
        ordered = sorted(self.parameter_list.keys())
        for name in ordered:
            vals = self.get_parameter(name)['values']
            args.append(name)
            values.append(vals)

        index = 0
        for element in itertools.product(*values):
            desc = dict(itertools.izip(args, element))
            fail = False
            for k,v in q.items():
                if desc[k] != v:
                    fail = True
            if not fail:
                yield self._load_slice(q, index, desc)
            index = index + 1


def make_parameter(name, values, **kwargs):
    default = kwargs['default'] if 'default' in kwargs else values[0]
    if not default in values:
        raise RuntimeError, "Invalid default, must be one of %s" % str(values)

    typechoice = kwargs['typechoice'] if 'typechoice' in kwargs else 'range'
    valid_types = ['list','range','option','hidden']
    if not typechoice in valid_types:
        raise RuntimeError, "Invalid typechoice, must be one of %s" % str(valid_types)

    label = kwargs['label'] if 'label' in kwargs else name

    properties = dict()
    properties['type'] = typechoice
    properties['label'] = label
    properties['values'] = values
    properties['default'] = default
    return properties

def make_field(name, _values, **kwargs):
    #specialization of make_parameters for parameters that define fields
    #in this case the values is a list of name, type pairs

    values = _values.keys()
    img_types = _values.values()
    valid_itypes = ['rgb','depth','value','luminance','normals']
    for i in img_types:
        if i not in valid_itypes:
            raise RuntimeError, "Invalid typechoice, must be one of %s" % str(valid_itypes)

    default = kwargs['default'] if 'default' in kwargs else values[0]
    if not default in values:
        raise RuntimeError, "Invalid default, must be one of %s" % str(values)

    typechoice = 'hidden'
    valid_types = ['list','range','option','hidden']
    if not typechoice in valid_types:
        raise RuntimeError, "Invalid typechoice, must be one of %s" % str(valid_types)

    label = kwargs['label'] if 'label' in kwargs else name

    properties = dict()
    properties['type'] = typechoice
    properties['label'] = label
    properties['values'] = values
    properties['default'] = default
    properties['types'] = img_types
    return properties
