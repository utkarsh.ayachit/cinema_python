.. CinemaPython documentation master file, created by
   sphinx-quickstart on Mon Jun  1 16:54:14 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CinemaPython's documentation!
========================================

Extreme scale scientific simulations are leading a charge to exascale
computation, and data analytics runs the risk of being a bottleneck to
scientific discovery. Due to power and I/O constraints, we expect *in situ*
visualization and analysis will be a critical component of these workflows.
Options for extreme scale data analysis are often presented as a stark contrast:
write large files to disk for interactive, exploratory analysis, or perform in
situ analysis to save detailed data about phenomena that a scientists knows
about in advance. We present a novel framework for a third option – a highly
interactive, image-based approach that promotes exploration of simulation
results, and is easily accessed through extensions to widely used open source
tools. This *in situ* approach supports interactive exploration of a wide range of
results, while still significantly reducing data movement and storage.

More information about the overall design of Cinema is available in the paper,
An Image-based Approach to Extreme Scale In Situ Visualization and Analysis,
which is available at the following link:
`https://datascience.lanl.gov/data/papers/SC14.pdf <https://datascience.lanl.gov/data/papers/SC14.pdf>`_

The cinema_python module provides a set of APIs that make it easy to read
and write Cinema databases of various specs and formats. Our goal is to grow
this API as new functionality is added to Cinema by the community.

Requirements
------------

* Python 2.x
* numpy
* PIL (not required for output if VTK is available)
* VTK (optional)
* ParaView (optional)

Basic Usage
-----------

If you have numpy, PIL and VTK installed, you can use one of the tests
to generate a simple Cinema database as follows.::

    >>> from cinema_python import tests
    >>> tests.test_vtk_clip("./info.json")

This should generate a number of png files and one json files that looks
similar to this::

    {"associations": {}, "arguments": {"theta": {"default": -180, "values": [-180, -140, -100, -60, -20, 20, 60, 100, 140, 180], "type": "range", "label": "theta"}, "phi": {"default": 0, "values": [0, 40, 80, 120, 160], "type": "range", "label": "phi"}, "offset": {"default": 0, "values": [0, 0.2, 0.4, 0.6, 0.8, 1.0], "type": "range", "label": "offset"}}, "name_pattern": "{phi}_{theta}_{offset}_slice.png", "metadata": {"type": "parametric-image-stack"}}

For details about the Cinema spec used, see the `Cinema specs repo <https://gitlab.kitware.com/cinema/specs>`_.

You can view the generated database using the `Python Qt viewer <https://gitlab.kitware.com/cinema/qt-viewer>`_ or the
`basic Web viewer <https://gitlab.kitware.com/cinema/basic-web-viewer>`_.

Here is a simplified version of the code from `test_vtk_clip`::

    import explorers
    import vtk_explorers
    import vtk

    # Setup a VTK pipeline
    s = vtk.vtkSphereSource()

    plane = vtk.vtkPlane()
    plane.SetOrigin(0, 0, 0)
    plane.SetNormal(-1, -1, 0)

    clip = vtk.vtkClipPolyData()
    clip.SetInputConnection(s.GetOutputPort())
    clip.SetClipFunction(plane)
    clip.GenerateClipScalarsOn()
    clip.GenerateClippedOutputOn()
    clip.SetValue(0)

    m = vtk.vtkPolyDataMapper()
    m.SetInputConnection(clip.GetOutputPort())

    rw = vtk.vtkRenderWindow()
    r = vtk.vtkRenderer()
    rw.AddRenderer(r)

    a = vtk.vtkActor()
    a.SetMapper(m)
    r.AddActor(a)

    # make or open a cinema data store to put results in
    cs = FileStore(fname)
    # The png files created will be named using this pattern
    cs.filename_pattern = "{phi}_{theta}_{offset}_slice.png"
    # We add 3 parameters: 2 camera angles & the slice offset
    cs.add_parameter("phi", make_parameter('phi', range(0, 200, 40)))
    cs.add_parameter("theta", make_parameter('theta', range(-180,200,40)))
    cs.add_parameter("offset", make_parameter('offset', [0,.2,.4,.6,.8,1.0]))

    # This explorer will change the camera parameters (phi and theta) during generation
    cam = vtk_explorers.Camera([0,0,0], [0,1,0], 3.0, r.GetActiveCamera()) #phi,theta implied
    # Changes the slice offset
    g = vtk_explorers.Clip('offset', clip)
    # Combines all explorers to generate the images
    e = vtk_explorers.ImageExplorer(cs, ['offset','phi', 'theta'], [cam, g], rw)

    # run through all parameter combinations and put data into the store
    e.explore()

API Documentation
------------------

.. toctree::
   :maxdepth: 4

   cinema_python


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

